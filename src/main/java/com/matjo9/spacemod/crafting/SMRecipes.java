package com.matjo9.spacemod.crafting;

import com.matjo9.spacemod.init.SMBlocks;
import com.matjo9.spacemod.init.SMItems;
import com.matjo9.spacemod.util.LogHelper;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.registry.GameRegistry;


public class SMRecipes {

    public static void initRecipes() {

        Block currentBlock = SMBlocks.copperBlock;
        Item currentItem = SMItems.copperIngot;

        /*
         * GameRegistry.addShapedRecipe(new ItemStack(SMBlocks.copperBlock), new
         * Object [] { "###", "###", "###", '#', SMItems.copperIngot });
         */
        for (int i = 0; i < 8; i++) {

            switch (i) {
                case 0:
                    currentBlock = SMBlocks.copperBlock;
                    currentItem = SMItems.copperIngot;
                    break;
                case 1:
                    currentBlock = SMBlocks.aluminumBlock;
                    currentItem = SMItems.aluminumIngot;
                    break;
                case 2:
                    currentBlock = SMBlocks.bronzeBlock;
                    currentItem = SMItems.bronzeIngot;
                    break;
                case 3:
                    currentBlock = SMBlocks.manganeseBlock;
                    currentItem = SMItems.manganeseIngot;
                    break;
                case 4:
                    currentBlock = SMBlocks.vanadiumBlock;
                    currentItem = SMItems.vanadiumIngot;
                    break;
                case 5:
                    currentBlock = SMBlocks.nickelBlock;
                    currentItem = SMItems.nickelIngot;
                    break;
                case 6:
                    currentBlock = SMBlocks.tinBlock;
                    currentItem = SMItems.tinIngot;
                    break;
                case 7:
                    currentBlock = SMBlocks.zincBlock;
                    currentItem = SMItems.zincIngot;
                    break;
            }

            GameRegistry.addShapedRecipe(new ItemStack(currentBlock), new Object[] {
                    "###", "###", "###", '#', currentItem
            });
            LogHelper.debug("RegisterRecipe Block: " + (i + 1));

        }

        GameRegistry.addShapelessRecipe(new ItemStack(SMItems.bronzeIngot), new Object[] {
                SMItems.aluminumIngot, SMItems.chickenShit, SMBlocks.aluminumBlock
        });

    }

}
