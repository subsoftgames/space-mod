package com.matjo9.spacemod.crafting;

import com.matjo9.spacemod.init.SMBlocks;
import com.matjo9.spacemod.init.SMItems;

import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.registry.GameRegistry;


public class SMSmelting {

    public static void initSmelting() {

        // Smelting block input itemstack output
        GameRegistry.addSmelting(SMBlocks.copperOre, new ItemStack(SMItems.copperIngot), 1);
        GameRegistry.addSmelting(SMBlocks.manganeseOre, new ItemStack(SMItems.manganeseIngot), 1);
        GameRegistry.addSmelting(SMBlocks.nickelOre, new ItemStack(SMItems.nickelIngot), 1);
        GameRegistry.addSmelting(SMBlocks.tinOre, new ItemStack(SMItems.tinIngot), 1);
        GameRegistry.addSmelting(SMBlocks.vanadiumOre, new ItemStack(SMItems.vanadiumIngot), 1);
        GameRegistry.addSmelting(SMBlocks.zincOre, new ItemStack(SMItems.zincIngot), 1);

        GameRegistry.addSmelting(SMBlocks.netherCopperOre, new ItemStack(SMItems.copperIngot), 1);
        GameRegistry.addSmelting(SMBlocks.netherManganeseOre, new ItemStack(SMItems.manganeseIngot), 1);
        GameRegistry.addSmelting(SMBlocks.netherNickelOre, new ItemStack(SMItems.nickelIngot), 1);
        GameRegistry.addSmelting(SMBlocks.netherTinOre, new ItemStack(SMItems.tinIngot), 1);
        GameRegistry.addSmelting(SMBlocks.netherVanadiumOre, new ItemStack(SMItems.zincIngot), 1);
        GameRegistry.addSmelting(SMBlocks.netherZincOre, new ItemStack(SMItems.zincIngot), 1);

    }

}
