package com.matjo9.spacemod.reference;

import com.matjo9.spacemod.CreativeTabs.SMArmorTab;
import com.matjo9.spacemod.CreativeTabs.SMBlocksTab;
import com.matjo9.spacemod.CreativeTabs.SMDebugTab;
import com.matjo9.spacemod.CreativeTabs.SMItemsTab;
import com.matjo9.spacemod.CreativeTabs.SMToolsTab;


/**
 * Created by degou on 13-2-2016.
 */
public class Reference {

    public static final boolean isDebug = true;

    public static final String MOD_ID = "sm";
    public static final String MOD_NAME = "Space Mod";
    public static final String VERSION = "V 0.1";

    public static final String SM_CLIENT_PROXY = "com.matjo9.spacemod.proxy.ClientProxy";
    public static final String SM_COMMON_PROXY = "com.matjo9.spacemod.proxy.CommonProxy";

    public static final SMItemsTab smItemsTab = new SMItemsTab("smItemsTab");
    public static final SMBlocksTab smBlocksTab = new SMBlocksTab("smBlocksTab");
    public static final SMToolsTab smToolsTab = new SMToolsTab("smToolsTab");
    public static final SMArmorTab smArmorTab = new SMArmorTab("smArmorTab");
    public static final SMDebugTab smDebugTab = new SMDebugTab("smDebugTab");
}
