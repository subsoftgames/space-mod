package com.matjo9.spacemod;

import com.matjo9.spacemod.proxy.CommonProxy;
import com.matjo9.spacemod.reference.Reference;
import com.matjo9.spacemod.util.LogHelper;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;


@Mod(modid = Reference.MOD_ID, name = Reference.MOD_NAME, version = Reference.VERSION)
public class Main {

    @Mod.Instance(Reference.MOD_ID)
    public static Main instance;

    @SidedProxy(clientSide = Reference.SM_CLIENT_PROXY, serverSide = Reference.SM_COMMON_PROXY)
    public static CommonProxy proxy;

    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent preEvent) {

        LogHelper.debug("PreInitialization Started");

        Main.proxy.preInit(preEvent);

        LogHelper.debug("PreInitialization Ended");
    }

    @Mod.EventHandler
    public void init(FMLInitializationEvent event) {

        LogHelper.debug("Initialization Started");
        Main.proxy.init(event);

        LogHelper.debug("Initialization Ended");
    }

    @Mod.EventHandler
    public void postInit(FMLPostInitializationEvent postEvent) {

        LogHelper.debug("PostInitialization Started");
        Main.proxy.postInit(postEvent);

        LogHelper.debug("PostInitialization Ended");
    }

}