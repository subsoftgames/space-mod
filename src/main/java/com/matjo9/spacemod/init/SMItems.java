package com.matjo9.spacemod.init;

import com.matjo9.spacemod.items.SMItem;
import com.matjo9.spacemod.items.armor.SMVanadiumArmor;
import com.matjo9.spacemod.items.tools.SMPitchHarvester;
import com.matjo9.spacemod.items.tools.SMShaleMattock;
import com.matjo9.spacemod.items.tools.SMVaanadiumSpade;
import com.matjo9.spacemod.items.tools.SMVanadiumAxe;
import com.matjo9.spacemod.items.tools.SMVanadiumHoe;
import com.matjo9.spacemod.items.tools.SMVanadiumPickaxe;
import com.matjo9.spacemod.items.tools.SMVanadiumSword;
import com.matjo9.spacemod.reference.Reference;

import net.minecraft.item.Item;
import net.minecraft.item.Item.ToolMaterial;
import net.minecraft.item.ItemArmor.ArmorMaterial;
import net.minecraftforge.common.util.EnumHelper;
import net.minecraftforge.fml.common.registry.GameRegistry;


public class SMItems {

    // Tools
    public static ToolMaterial VANADIUMTOOLS = EnumHelper.addToolMaterial("VANADIUMTOOLS", 3, 800, 8.0F, 4.0F, 10);

    public static ToolMaterial GELTOOLS = EnumHelper.addToolMaterial("GELTOOLS", 0, 300, 8.0F, 0.0F, 0);
    public static ToolMaterial PITCHTOOLS = EnumHelper.addToolMaterial("PITCHTOOLS", 0, 300, 8.0F, 0.0F, 0);

    public static Item vanadiumPickaxe;
    public static Item vanadiumAxe;
    public static Item vanadiumSpade;
    public static Item vanadiumHoe;
    public static Item vanadiumSword;

    public static Item shaleMattock; // Like an spade
    public static Item pitchHarvester; // Like an axe

    // Armor
    public static ArmorMaterial VANADIUMARMOR = EnumHelper.addArmorMaterial("VANADIUMARMOR", Reference.MOD_ID + ":" + "vanadium", 20, new int[] {
            2, 5, 3, 1
    }, 30);

    public static Item vanadiumHelmet;
    public static Item vanadiumChestplate;
    public static Item vanadiumLeggings;
    public static Item vanadiumBoots;

    // Ingots
    public static Item copperIngot;
    public static Item nickelIngot;
    public static Item vanadiumIngot;
    public static Item tinIngot;
    public static Item manganeseIngot;
    public static Item zincIngot;

    // Gems
    public static Item topazStone;

    // Alloys
    public static Item bronzeIngot;
    public static Item aluminumIngot;

    // Fuels
    public static Item treePitch;
    public static Item viscinousGel;

    // Fertilizers
    public static Item chickenShit;

    public static void initItems() {

        // Tools
        GameRegistry.registerItem(vanadiumPickaxe = new SMVanadiumPickaxe("vanadiumPickaxe", VANADIUMTOOLS), "vanadiumPickaxe");
        GameRegistry.registerItem(vanadiumAxe = new SMVanadiumAxe("vanadiumAxe", VANADIUMTOOLS), "vanadiumAxe");
        GameRegistry.registerItem(vanadiumSpade = new SMVaanadiumSpade("vanadiumSpade", VANADIUMTOOLS), "vanadiumSpade");
        GameRegistry.registerItem(vanadiumHoe = new SMVanadiumHoe("vanadiumHoe", VANADIUMTOOLS), "vanadiumHoe");
        GameRegistry.registerItem(vanadiumSword = new SMVanadiumSword("vanadiumSword", VANADIUMTOOLS), "vanadiumSword");

        GameRegistry.registerItem(shaleMattock = new SMShaleMattock("shaleMattock", GELTOOLS), "shaleMattock");
        GameRegistry.registerItem(pitchHarvester = new SMPitchHarvester("pitchHarvester", PITCHTOOLS), "pitchHarvester");

        // Armor
        GameRegistry.registerItem(vanadiumHelmet = new SMVanadiumArmor("vanadiumHelmet", VANADIUMARMOR, 1, 0), "vanadiumHelmet");
        GameRegistry.registerItem(vanadiumChestplate = new SMVanadiumArmor("vanadiumChestplate", VANADIUMARMOR, 2, 1), "vanadiumChestplate");
        GameRegistry.registerItem(vanadiumLeggings = new SMVanadiumArmor("vanadiumLeggings", VANADIUMARMOR, 3, 2), "vanadiumLeggings");
        GameRegistry.registerItem(vanadiumBoots = new SMVanadiumArmor("vanadiumBoots", VANADIUMARMOR, 4, 3), "vanadiumBoots");

        // Ingots
        GameRegistry.registerItem(copperIngot = new SMItem("copperIngot"), "copperIngot");
        GameRegistry.registerItem(nickelIngot = new SMItem("nickelIngot"), "nickelIngot");
        GameRegistry.registerItem(vanadiumIngot = new SMItem("vanadiumIngot"), "vanadiumIngot");
        GameRegistry.registerItem(tinIngot = new SMItem("tinIngot"), "tinIngot");
        GameRegistry.registerItem(manganeseIngot = new SMItem("manganeseIngot"), "manganeseIngot");
        GameRegistry.registerItem(zincIngot = new SMItem("zincIngot"), "zincIngot");

        // Gems
        GameRegistry.registerItem(topazStone = new SMItem("topazStone"), "topazStone");

        // Alloys
        GameRegistry.registerItem(bronzeIngot = new SMItem("bronzeIngot"), "bronzeIngot");
        GameRegistry.registerItem(aluminumIngot = new SMItem("aluminumIngot"), "aluminumIngot");

        // Fuels
        GameRegistry.registerItem(treePitch = new SMItem("treePitch"), "treePitch");
        GameRegistry.registerItem(viscinousGel = new SMItem("viscinousGel"), "viscinousGel");

        // Fertilizers
        GameRegistry.registerItem(chickenShit = new SMItem("chickenShit"), "chickenShit");

    }

}
