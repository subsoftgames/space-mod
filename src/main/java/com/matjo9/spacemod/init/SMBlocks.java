package com.matjo9.spacemod.init;

import com.matjo9.spacemod.blocks.SMBlock;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraftforge.fml.common.registry.GameRegistry;


/**
 * Created by degou on 13-2-2016.
 */
public class SMBlocks {

    // Ores
    public static Block copperOre;
    public static Block nickelOre;
    public static Block manganeseOre;
    public static Block tinOre;
    public static Block vanadiumOre;
    public static Block zincOre;

    // Gems
    public static Block topazOre;

    // Blocks
    public static Block copperBlock;
    public static Block nickelBlock;
    public static Block manganeseBlock;
    public static Block tinBlock;
    public static Block vanadiumBlock;
    public static Block zincBlock;

    public static Block aluminumBlock;
    public static Block bronzeBlock;

    // Nether Ores
    public static Block netherCopperOre;
    public static Block netherNickelOre;
    public static Block netherManganeseOre;
    public static Block netherTinOre;
    public static Block netherVanadiumOre;
    public static Block netherZincOre;

    public static void initBlocks() {

        // Ores
        GameRegistry.registerBlock(copperOre = new SMBlock("copperOre", Material.rock, 3, 15), "copperOre");
        GameRegistry.registerBlock(nickelOre = new SMBlock("nickelOre", Material.rock, 3, 15), "nickelOre");
        GameRegistry.registerBlock(manganeseOre = new SMBlock("manganeseOre", Material.rock, 3, 15), "manganeseOre");
        GameRegistry.registerBlock(tinOre = new SMBlock("tinOre", Material.rock, 3, 15), "tinOre");
        GameRegistry.registerBlock(vanadiumOre = new SMBlock("vanadiumOre", Material.rock, 3, 15), "vanadiumOre");
        GameRegistry.registerBlock(zincOre = new SMBlock("zincOre", Material.rock, 3, 15), "zincOre");

        // Gems
        GameRegistry.registerBlock(topazOre = new SMBlock("topazOre", Material.rock, 3, 15), "topazOre");

        // Nether Ores
        GameRegistry.registerBlock(netherCopperOre = new SMBlock("netherCopperOre", Material.rock, 3, 15), "netherCopperOre");
        GameRegistry.registerBlock(netherNickelOre = new SMBlock("netherNickelOre", Material.rock, 3, 15), "netherNickelOre");
        GameRegistry.registerBlock(netherManganeseOre = new SMBlock("netherManganeseOre", Material.rock, 3, 15), "netherManganeseOre");
        GameRegistry.registerBlock(netherTinOre = new SMBlock("netherTinOre", Material.rock, 3, 15), "netherTinOre");
        GameRegistry.registerBlock(netherVanadiumOre = new SMBlock("netherVanadiumOre", Material.rock, 3, 15), "netherVanadiumOre");
        GameRegistry.registerBlock(netherZincOre = new SMBlock("netherZincOre", Material.rock, 3, 15), "netherZincOre");

        // Blocks
        GameRegistry.registerBlock(copperBlock = new SMBlock("copperBlock", Material.rock, 3, 15), "copperBlock");
        GameRegistry.registerBlock(nickelBlock = new SMBlock("nickelBlock", Material.rock, 3, 15), "nickelBlock");
        GameRegistry.registerBlock(manganeseBlock = new SMBlock("manganeseBlock", Material.rock, 3, 15), "manganeseBlock");
        GameRegistry.registerBlock(tinBlock = new SMBlock("tinBlock", Material.rock, 3, 15), "tinBlock");
        GameRegistry.registerBlock(vanadiumBlock = new SMBlock("vanadiumBlock", Material.rock, 3, 15), "vanadiumBlock");
        GameRegistry.registerBlock(zincBlock = new SMBlock("zincBlock", Material.rock, 3, 15), "zincBlock");

        GameRegistry.registerBlock(aluminumBlock = new SMBlock("aluminumBlock", Material.rock, 3, 15), "aluminumBlock");
        GameRegistry.registerBlock(bronzeBlock = new SMBlock("bronzeBlock", Material.rock, 3, 15), "bronzeBlock");
    }

}
