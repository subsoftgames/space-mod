package com.matjo9.spacemod.items.tools;

import com.matjo9.spacemod.reference.Reference;

import net.minecraft.item.ItemSpade;


public class SMShaleMattock extends ItemSpade {

    public SMShaleMattock(String unlocalizedName, ToolMaterial material) {
        super(material);
        this.setUnlocalizedName(unlocalizedName);
        this.setCreativeTab(Reference.smToolsTab);
    }

}
