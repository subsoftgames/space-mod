package com.matjo9.spacemod.items.tools;

import com.matjo9.spacemod.reference.Reference;

import net.minecraft.item.ItemPickaxe;


public class SMVanadiumPickaxe extends ItemPickaxe {

    public SMVanadiumPickaxe(String unlocalizedName, ToolMaterial material) {
        super(material);
        this.setUnlocalizedName(unlocalizedName);
        this.setCreativeTab(Reference.smToolsTab);
    }

}
