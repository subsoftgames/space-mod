package com.matjo9.spacemod.items.tools;

import com.matjo9.spacemod.reference.Reference;

import net.minecraft.item.ItemHoe;


public class SMVanadiumHoe extends ItemHoe {

    public SMVanadiumHoe(String unlocalizedName, ToolMaterial material) {
        super(material);
        this.setUnlocalizedName(unlocalizedName);
        this.setCreativeTab(Reference.smToolsTab);
    }

}
