package com.matjo9.spacemod.items.tools;

import com.matjo9.spacemod.reference.Reference;

import net.minecraft.item.ItemAxe;


public class SMVanadiumAxe extends ItemAxe {

    public SMVanadiumAxe(String unlocalizedName, ToolMaterial material) {
        super(material);
        this.setUnlocalizedName(unlocalizedName);
        this.setCreativeTab(Reference.smToolsTab);
    }

}
