package com.matjo9.spacemod.items.tools;

import com.matjo9.spacemod.reference.Reference;

import net.minecraft.item.ItemSword;


public class SMVanadiumSword extends ItemSword {

    public SMVanadiumSword(String unlocalizedName, ToolMaterial material) {
        super(material);
        this.setUnlocalizedName(unlocalizedName);
        this.setCreativeTab(Reference.smToolsTab);
    }

}
