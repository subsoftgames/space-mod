package com.matjo9.spacemod.items.armor;

import com.matjo9.spacemod.init.SMItems;
import com.matjo9.spacemod.reference.Reference;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.world.World;


public class SMVanadiumArmor extends ItemArmor {

    public SMVanadiumArmor(String unlocalizedName, ArmorMaterial material, int renderIndex, int armorType) {
        super(material, renderIndex, armorType);
        this.setUnlocalizedName(unlocalizedName);
        this.setCreativeTab(Reference.smArmorTab);

    }

    @Override
    public void onArmorTick(World world, EntityPlayer player, ItemStack itemstack) {

        if (itemstack.getItem() == SMItems.vanadiumHelmet) {
            this.effectPlayer(player, Potion.nightVision, 500, 0);
        }
        if (itemstack.getItem() == SMItems.vanadiumChestplate) {
            this.effectPlayer(player, Potion.absorption, 1);
        }
        if (itemstack.getItem() == SMItems.vanadiumLeggings) {
            this.effectPlayer(player, Potion.moveSpeed, 1);
        }
        if (itemstack.getItem() == SMItems.vanadiumBoots) {
            this.effectPlayer(player, Potion.jump, 2);
        }

        if (this.isWearingFullSet(player, SMItems.vanadiumHelmet, SMItems.vanadiumChestplate, SMItems.vanadiumLeggings, SMItems.vanadiumBoots)) {
            this.effectPlayer(player, Potion.regeneration, 1);
        }

    }

    private boolean isWearingFullSet(EntityPlayer player, Item helmet, Item chestplate, Item leggings, Item boots) {

        return player.inventory.armorItemInSlot(3) != null && player.inventory.armorItemInSlot(3).getItem() == helmet && player.inventory.armorItemInSlot(2) != null
                && player.inventory.armorItemInSlot(2).getItem() == chestplate && player.inventory.armorItemInSlot(1) != null && player.inventory.armorItemInSlot(1).getItem() == leggings
                && player.inventory.armorItemInSlot(0) != null && player.inventory.armorItemInSlot(0).getItem() == boots;
    }

    private void effectPlayer(EntityPlayer player, Potion potion, int amlpifier) {

        if (player.getActivePotionEffect(potion) == null || player.getActivePotionEffect(potion).getDuration() <= 11) {
            player.addPotionEffect(new PotionEffect(potion.id, 159, amlpifier, true, true));
        }

    }

    private void effectPlayer(EntityPlayer player, Potion potion, int duration, int amlpifier) {

        if (player.getActivePotionEffect(potion) == null || player.getActivePotionEffect(potion).getDuration() <= 11) {
            player.addPotionEffect(new PotionEffect(potion.id, duration, amlpifier, true, true));
        }
    }

}
