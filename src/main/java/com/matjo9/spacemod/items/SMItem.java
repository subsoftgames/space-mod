package com.matjo9.spacemod.items;

import com.matjo9.spacemod.reference.Reference;

import net.minecraft.item.Item;


public class SMItem extends Item {

    public SMItem(String name) {
        super();

        this.setUnlocalizedName(name);
        this.setCreativeTab(Reference.smItemsTab);

    }

}
