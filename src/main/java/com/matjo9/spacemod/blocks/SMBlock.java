package com.matjo9.spacemod.blocks;

import java.util.Random;

import com.matjo9.spacemod.init.SMBlocks;
import com.matjo9.spacemod.init.SMItems;
import com.matjo9.spacemod.reference.Reference;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.item.Item;


/**
 * Created by degou on 13-2-2016.
 */
public class SMBlock extends Block {

    public SMBlock(String unlocalizedName, Material material, float hardness, float resistance) {
        super(material);

        this.setCreativeTab(Reference.smBlocksTab);
        this.setUnlocalizedName(unlocalizedName);
        this.setHardness(hardness);
        this.setResistance(resistance);
    }

    @Override
    public Item getItemDropped(IBlockState state, Random rand, int fortune) {

        return this == SMBlocks.topazOre ? SMItems.topazStone : Item.getItemFromBlock(this);
    }

    @Override
    public int quantityDropped(Random rand) {

        return this == SMBlocks.topazOre ? 4 + rand.nextInt(5) : 1;

    }

}
