package com.matjo9.spacemod.proxy;

import com.matjo9.spacemod.crafting.SMRecipes;
import com.matjo9.spacemod.crafting.SMSmelting;
import com.matjo9.spacemod.handler.SMDropHandler;
import com.matjo9.spacemod.handler.SMFuelHandler;
import com.matjo9.spacemod.init.SMBlocks;
import com.matjo9.spacemod.init.SMItems;
import com.matjo9.spacemod.util.LogHelper;

import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.registry.GameRegistry;


/**
 * Created by degou on 13-2-2016.
 */
public class CommonProxy {

    public void preInit(FMLPreInitializationEvent preEvent) {

        LogHelper.debug("Common Proxy PreInitialization Started");

        LogHelper.info("RegisterBlocks");
        SMBlocks.initBlocks();

        LogHelper.info("RegisterItems");
        SMItems.initItems();

        LogHelper.info("RegisterRecipes");
        SMRecipes.initRecipes();

        LogHelper.info("RegisterSmelting Recipes");
        SMSmelting.initSmelting();

        LogHelper.debug("Common Proxy PreInitialization Ended");
    }

    public void init(FMLInitializationEvent event) {

        LogHelper.debug("Common Proxy Initialization Started");

        LogHelper.info("RegisterEventHandler: MinecraftForge_EVENT_BUS: SMDropHandler");
        MinecraftForge.EVENT_BUS.register(new SMDropHandler());

        LogHelper.info("RegisterFuelHandler: SMFuelHandler");
        GameRegistry.registerFuelHandler(new SMFuelHandler());
        
        LogHelper.debug("Common Proxy Initialization Ended");
    }

    public void postInit(FMLPostInitializationEvent postEvent) {

        LogHelper.debug("Common Proxy PostInitialization Started");

        LogHelper.debug("Common Proxy PostInitialization Ended");
    }
}
