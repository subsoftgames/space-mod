package com.matjo9.spacemod.proxy;

import com.matjo9.spacemod.render.SMBlockRender;
import com.matjo9.spacemod.render.SMItemRender;
import com.matjo9.spacemod.util.LogHelper;

import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;


/**
 * Created by degou on 13-2-2016.
 */
public class ClientProxy extends CommonProxy {

    @Override
    public void preInit(FMLPreInitializationEvent preEvent) {

        LogHelper.debug("Client Proxy PreInitialization Started");

        super.preInit(preEvent);

        LogHelper.debug("Client Proxy PreInitialization Ended");
    }

    @Override
    public void init(FMLInitializationEvent event) {

        LogHelper.debug("Client Proxy Initialization Started");

        super.init(event);

        LogHelper.info("RegisterRenderer: Blocks");
        SMBlockRender.registerBlockRender();

        LogHelper.info("RegisterRenderer: Items");
        SMItemRender.registerItemRender();

        LogHelper.debug("Client Proxy Initialization Ended");
    }

    @Override
    public void postInit(FMLPostInitializationEvent postEvent) {

        LogHelper.debug("Client Proxy PostInitialization Started");

        super.postInit(postEvent);

        LogHelper.debug("Client Proxy PostInitialization Ended");
    }

}
