package com.matjo9.spacemod.handler;

import java.util.Random;

import com.matjo9.spacemod.init.SMItems;
import com.matjo9.spacemod.items.tools.SMPitchHarvester;
import com.matjo9.spacemod.items.tools.SMShaleMattock;

import net.minecraft.block.BlockOldLog;
import net.minecraft.block.BlockPlanks;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.passive.EntityChicken;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemStack;
import net.minecraftforge.event.entity.living.LivingDropsEvent;
import net.minecraftforge.event.world.BlockEvent.HarvestDropsEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;


public class SMDropHandler {

    @SubscribeEvent
    public void addEntityDrop(LivingDropsEvent event) {

        if (event.entity instanceof EntityChicken) {
            ItemStack itemstack = new ItemStack(SMItems.chickenShit, 1);
            event.drops.add(new EntityItem(event.entity.worldObj, event.entity.posX, event.entity.posY, event.entity.posZ, itemstack));
        }
    }

    @SubscribeEvent
    public void addBlockDrop(HarvestDropsEvent event) {

        if (event.state.getBlock() == Blocks.gravel) {

            Random rand = new Random();
            ItemStack holding = event.harvester.inventory.getStackInSlot(event.harvester.inventory.currentItem);
            if (holding != null && holding.getItem() instanceof SMShaleMattock) {
                event.drops.clear();
                event.drops.add(new ItemStack(SMItems.viscinousGel, rand.nextInt(4)));
            }
        }

        if (event.state.getBlock() == Blocks.log) {
            if (event.state.getValue(BlockOldLog.VARIANT) == BlockPlanks.EnumType.SPRUCE) {
                ItemStack holding = event.harvester.inventory.getStackInSlot(event.harvester.inventory.currentItem);
                if (holding != null && holding.getItem() instanceof SMPitchHarvester) {
                    event.drops.clear();
                    event.drops.add(new ItemStack(SMItems.treePitch, 2));
                }
            }
        }
    }
}
