package com.matjo9.spacemod.handler;

import com.matjo9.spacemod.init.SMItems;

import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.IFuelHandler;

public class SMFuelHandler implements IFuelHandler{

    @Override
    public int getBurnTime(ItemStack fuel) {
        if(fuel.getItem() == SMItems.treePitch) return 800;
        if(fuel.getItem() == SMItems.viscinousGel) return 2400;
        return 0;
    }

}
