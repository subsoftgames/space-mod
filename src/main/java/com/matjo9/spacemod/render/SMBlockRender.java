package com.matjo9.spacemod.render;

import com.matjo9.spacemod.init.SMBlocks;
import com.matjo9.spacemod.reference.Reference;

import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.model.ModelResourceLocation;
import net.minecraft.item.Item;


/**
 * Created by degou on 13-2-2016.
 */
public final class SMBlockRender {

    public static void registerBlockRender() {

        // Ores
        regBlock(SMBlocks.copperOre);
        regBlock(SMBlocks.nickelOre);
        regBlock(SMBlocks.manganeseOre);
        regBlock(SMBlocks.tinOre);
        regBlock(SMBlocks.vanadiumOre);
        regBlock(SMBlocks.zincOre);

        // Gems
        regBlock(SMBlocks.topazOre);

        // Blocks
        regBlock(SMBlocks.copperBlock);
        regBlock(SMBlocks.nickelBlock);
        regBlock(SMBlocks.manganeseBlock);
        regBlock(SMBlocks.tinBlock);
        regBlock(SMBlocks.vanadiumBlock);
        regBlock(SMBlocks.zincBlock);

        regBlock(SMBlocks.aluminumBlock);
        regBlock(SMBlocks.bronzeBlock);

        // Nether Ores
        regBlock(SMBlocks.netherCopperOre);
        regBlock(SMBlocks.netherNickelOre);
        regBlock(SMBlocks.netherManganeseOre);
        regBlock(SMBlocks.netherTinOre);
        regBlock(SMBlocks.netherVanadiumOre);
        regBlock(SMBlocks.netherZincOre);

    }

    public static void regBlock(Block block) {

        Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(Item.getItemFromBlock(block), 0,
                new ModelResourceLocation(Reference.MOD_ID + ":" + block.getUnlocalizedName().substring(5), "inventory"));
    }

}
