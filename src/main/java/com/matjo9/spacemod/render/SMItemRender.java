package com.matjo9.spacemod.render;

import com.matjo9.spacemod.init.SMItems;
import com.matjo9.spacemod.reference.Reference;

import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.model.ModelResourceLocation;
import net.minecraft.item.Item;


public class SMItemRender {

    public static void registerItemRender() {

        // Tools
        regItem(SMItems.vanadiumAxe);
        regItem(SMItems.vanadiumHoe);
        regItem(SMItems.vanadiumPickaxe);
        regItem(SMItems.vanadiumSpade);
        regItem(SMItems.vanadiumSword);

        regItem(SMItems.shaleMattock);
        regItem(SMItems.pitchHarvester);

        // Armor
        regItem(SMItems.vanadiumHelmet);
        regItem(SMItems.vanadiumChestplate);
        regItem(SMItems.vanadiumLeggings);
        regItem(SMItems.vanadiumBoots);

        // Ingots
        regItem(SMItems.copperIngot);
        regItem(SMItems.nickelIngot);
        regItem(SMItems.manganeseIngot);
        regItem(SMItems.tinIngot);
        regItem(SMItems.vanadiumIngot);
        regItem(SMItems.zincIngot);

        // Gems
        regItem(SMItems.topazStone);

        // Alloys
        regItem(SMItems.bronzeIngot);
        regItem(SMItems.aluminumIngot);

        // Fuels
        regItem(SMItems.viscinousGel);
        regItem(SMItems.treePitch);

        // Ferilizers
        regItem(SMItems.chickenShit);
    }

    public static void regItem(Item item) {

        Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(item, 0, new ModelResourceLocation(Reference.MOD_ID + ":" + item.getUnlocalizedName().substring(5), "inventory"));
    }

}
