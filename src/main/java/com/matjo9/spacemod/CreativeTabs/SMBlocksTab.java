package com.matjo9.spacemod.CreativeTabs;

import com.matjo9.spacemod.init.SMBlocks;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;


public class SMBlocksTab extends CreativeTabs {

    public SMBlocksTab(String label) {
        super(label);
    }

    @Override
    public Item getTabIconItem() {

        return Item.getItemFromBlock(SMBlocks.copperOre);
    }

}
