package com.matjo9.spacemod.CreativeTabs;

import com.matjo9.spacemod.init.SMItems;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;


public class SMItemsTab extends CreativeTabs {

    public SMItemsTab(String string) {
        super(string);

    }

    @Override
    public Item getTabIconItem() {

        return SMItems.vanadiumIngot;
    }

}
