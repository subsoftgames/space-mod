package com.matjo9.spacemod.CreativeTabs;

import com.matjo9.spacemod.init.SMItems;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;


public class SMToolsTab extends CreativeTabs {

    public SMToolsTab(String string) {
        super(string);

    }

    @Override
    public Item getTabIconItem() {

        return SMItems.vanadiumPickaxe;
    }

}
